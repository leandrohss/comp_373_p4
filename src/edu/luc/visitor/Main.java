package edu.luc.visitor;

public class Main {

	public static void main(String[] args) {
		new Main();
	}
	
	public Main() {
		
		
		Head head = new Head();
		Chest chest = new Chest();
		Arm leftArm = new Arm("left");
		Arm rightArm = new Arm("right");
		Leg leftLeg = new Leg("left");
		Leg rightLeg = new Leg("right");
		
		
		HumanBody human = new HumanBody(head, chest, leftArm, rightArm, leftLeg, rightLeg);
		
		BodyElementVisitor visitor = new BodyElementMoveVisitor();
		human.accept(visitor);
		
	}

}
