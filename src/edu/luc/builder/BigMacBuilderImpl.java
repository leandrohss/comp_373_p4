package edu.luc.builder;

public class BigMacBuilderImpl implements BigMacBuilder {
	
	private BigMac bigMac;
	
	public BigMacBuilderImpl() {
		bigMac = new BigMac();
	}
	
	public void buildBottomBun() {
		System.out.println("Building the bottom bun");
		bigMac.addIngredient("bottom bun");
	}
	
	public void buildPatties() {
		System.out.println("Building the patties");
		bigMac.addIngredient("patties");
	}
	
	public void buildOnions() {
		System.out.println("Building the onions");
		bigMac.addIngredient("onions");
	}
	
	public void buildMiddleBun() {
		System.out.println("Building the middle bun");
		bigMac.addIngredient("middle bun");
	}
	
	
	public void buildSpecialSauce() {
		System.out.println("Building the special sauce");
		bigMac.addIngredient("special sauce");
	}
	
	public void buildLettuce() {
		System.out.println("Building the lettuce");
		bigMac.addIngredient("lettuce");
	}
	
	public void buildTopBun() {
		System.out.println("Building the top bun");
		bigMac.addIngredient("top bun");
	}
	
	public BigMac getFood() {
		return bigMac;
	}
	
}
