package edu.luc.visitor;

public class BodyElementMoveVisitor implements BodyElementVisitor {

	@Override
	public void visit(Arm arm) {
		System.out.println("I'm moving my " + arm.getSide() + " hand");
	}

	@Override
	public void visit(Chest chest) {
		System.out.println("I'm moving my chest");
	}

	@Override
	public void visit(Leg leg) {
		System.out.println("I'm moving my " + leg.getSide() + " hand");
	}

	@Override
	public void visit(Head head) {
		System.out.println("I'm moving my head");
	}
	
	@Override
	public void visit(HumanBody head) {
		System.out.println("I'm moving all the body");
	}

}
