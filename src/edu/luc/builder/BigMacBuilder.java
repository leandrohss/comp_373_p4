package edu.luc.builder;

public interface BigMacBuilder {
	
	public void buildBottomBun();
	public void buildMiddleBun();
	public void buildTopBun();
	public void buildPatties();
	public void buildOnions();
	public void buildSpecialSauce();
	public void buildLettuce();
	public BigMac getFood();
	
}
