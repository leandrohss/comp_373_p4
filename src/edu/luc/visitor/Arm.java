package edu.luc.visitor;

public class Arm implements BodyElement {
	
	String side;
	
	public Arm(String side) {
		this.side = side;
	}
	
	public String getSide() {
		return side;
	}
	
	@Override
	public void accept(BodyElementVisitor visitor) {
		visitor.visit(this);
	}

}
