package edu.luc.visitor;

public interface BodyElementVisitor {

	
	public void visit(Arm arm);
	
	public void visit(Chest chest);
	
	public void visit(Leg leg);
	
	public void visit(Head head);
	
	public void visit(HumanBody head);

	
}
