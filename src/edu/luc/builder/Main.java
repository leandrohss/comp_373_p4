package edu.luc.builder;

public class Main {

	public static void main(String[] args) {
		new Main();
	}
	
	public Main() {
		
		BigMacBuilder builder = new BigMacBuilderImpl();
		
		McDonaldsDirector director = new McDonaldsDirector(builder);
		director.construct();
		
		BigMac bigMac = builder.getFood();
		
		System.out.println("\n\nThe client received a BigMac with:\n");
		for (String ingredient : bigMac.getIngredients()) {
			System.out.println("- " + ingredient );
		}
		
	}

}
