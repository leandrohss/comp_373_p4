package edu.luc.visitor;

import java.util.ArrayList;
import java.util.List;

public class HumanBody implements BodyElement {

	List<BodyElement> bodyElements;
	
	public HumanBody(Head head, Chest chest, Arm leftArm, Arm rightArm, Leg leftLeg, Leg rightLeg) {
		bodyElements = new ArrayList<>();
		
		bodyElements.add(head);
		bodyElements.add(chest);
		bodyElements.add(leftArm);
		bodyElements.add(rightArm);
		bodyElements.add(leftLeg);
		bodyElements.add(rightLeg);
	}
	
	
	@Override
	public void accept(BodyElementVisitor visitor) {
		for (BodyElement be : bodyElements) {
			be.accept(visitor);
		}
		
		visitor.visit(this);
		
	}
	
}
