package edu.luc.builder;

import java.util.ArrayList;
import java.util.List;

public class BigMac {

	List<String> ingredients;
	
	public BigMac() {
		ingredients = new ArrayList<>();
	}
	
	public void addIngredient(String ingredient) {
		ingredients.add(ingredient);
	}
	
	public List<String> getIngredients() {
		return ingredients;
	}
	
}
