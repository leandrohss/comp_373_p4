package edu.luc.visitor;

public class Leg implements BodyElement {
	
	String side;
	
	public Leg(String side) {
		this.side = side;
	}
	
	public String getSide() {
		return side;
	}
	
	@Override
	public void accept(BodyElementVisitor visitor) {
		visitor.visit(this);
	}
	
}
