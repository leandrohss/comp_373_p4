package edu.luc.builder;

public class McDonaldsDirector {

	BigMacBuilder builder;
	
	public McDonaldsDirector(BigMacBuilder builder) {
		this.builder = builder;
	}
	
	public void construct() {
		builder.buildBottomBun();
		builder.buildLettuce();
		builder.buildMiddleBun();
		builder.buildOnions();
		builder.buildPatties();
		builder.buildSpecialSauce();
		builder.buildTopBun();
	}
}
