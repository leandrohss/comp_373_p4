package edu.luc.visitor;

public interface BodyElement {

	public void accept(BodyElementVisitor visitor);
	
}
