package edu.luc.visitor;

public class Chest implements BodyElement {

	@Override
	public void accept(BodyElementVisitor visitor) {
		visitor.visit(this);
	}

}
